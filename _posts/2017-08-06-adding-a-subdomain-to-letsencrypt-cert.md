---
date: 2017-08-06 17:00:00 -0400
last_updated: 2018-03-18 17:00:00 -0400
title: Adding a new subdomain to a Let's Encrypt SSL certificate
slug: addding-subdomain-to-letsencrypt-cert
categories:
  - tech
tags:
  - letsencrypt
  - ssl
---

---

<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
Let's Encrypt now supports wildcard certificats, and there is an updated
post on Atomic Writing called "[Using a wildcard domain specification
with a Let's Encrypt SSL certificate][atomicwriting-wildcard-post]" that
goes over how to get a Let's Encrypt wildcard certificate.

---

In January of 2018, [Let's Encrypt will begin supporting wildcard
certificates][letsencrypt-wildcard]{:target="_blank"}. Until then, it's
still necessary to update a certificate file in order to add any new
subdomains.

This blog post is largely intended as a note to self about how to
update my Let's Encrypt certificate for my personal website. But if you,
like me, find yourself in the position of needing to expand your
certificate to include another subdomain, hopefully you will find this
post helpful.<!--read more-->

The first thing you will want to do is get some information about your
certificate, you can do this by running the following command:

```sh
sudo certbot certificates
```

This should give you some informative output about the certificates that
you've installed with `certbot`. The output should look something like
this:

```
-------------------------------------------------------------------------------
Found the following certs:
  Certificate Name: example.org
    Domains: example.org,git.example.org,shiny.example.org
    Expiry Date: 2017-11-14 22:10:00+00:00 (VALID: 79 days)
    Certificate Path: /etc/letsencrypt/live/example.org/fullchain.pem
    Private Key Path: /etc/letsencrypt/live/example.org/privkey.pem
-------------------------------------------------------------------------------
```

You'll want to use the `certonly` subcommand for `certbot` to modify
your certificate. You can specify which certificate with the
`--cert-name` flag. You'll also need to specify the `--webroot-path` (or
`-w`, for short), indicating where the files for the website actually
live. If this is multiple locations, you can use the `-w` flag multiple
times (see the manual pages for more information).

The next thing you'll want to do is indicate the domains for the
certificate; this includes all of the domains for which the certificate
is already valid as well as any new domains that you want to add to the
certificate.

For example, say that you wanted to add the subdomain `svn.example.org`
to the `example.org` certificate above. You can specify all old domains
and the new domain using the `--domains` flag (or `-d`, for short).  You
can either specify the domains with the `-d` flag as a comma-separated
list, or you can use multiple `-d` flags. Finally, since you're adding a
new subdomain, you'll also want to give the `--expand` flag to the
command.

Thus, putting it all together, you should run the following command to
add `svn.example.org` to your `example.org` certificate:

```sh
sudo certbot certonly --cert-name example.org -w /var/www/example -d example.org,git.example.org,shiny.example.org,svn.example.org --expand
```

You will be prompted about how you'd like to authenticate:

```
Saving debug log to /var/log/letsencrypt/letsencrypt.log

How would you like to authenticate with the ACME CA?
-------------------------------------------------------------------------------
1: Spin up a temporary webserver (standalone)
2: Place files in webroot directory (webroot)
-------------------------------------------------------------------------------
Select the appropriate number [1-2] then [enter] (press 'c' to cancel):
```

For this, I usually choose the second option.

Then, you'll also be prompted about whether you really meant to add new
domains to the certificate:

```
-------------------------------------------------------------------------------
You are updating certificate example.org to include domains: example.org,
git.example.org, shiny.example.org, svn.example.org

It previously included domains: example.org, git.example.org,
shiny.example.org

Did you intend to make this change?
-------------------------------------------------------------------------------
(U)pdate cert/(C)ancel:
```

Since this update is intentional, type <kbd>U</kbd> and hit
<kbd>ENTER</kbd>.

Finally, restart your web server. In my case, I
use [`nginx`][nginx]{:target="_blank"}, so restarting it looks like
this:

```sh
sudo /etc/init.d/nginx restart
```

Feel free to comment with any questions! I'm happy to try to help out as
best I can. 🤓&nbsp; However, like I said, this particular post is
largely intended as a note to self, and some aspects of this process
are dependent on your particular setup; nonetheless, I'm happy to help
if I can!

[atomicwriting-wildcard-post]: {{ site.baseurl }}{% post_url 2018-03-18-wildcard-certificate-with-letsencrypt %}
[letsencrypt-wildcard]: https://letsencrypt.org/2017/07/06/wildcard-certificates-coming-jan-2018.html
[nginx]: https://nginx.org/en/

<!-- Local Variables: -->
<!-- mode: markdown -->
<!-- coding: utf-8 -->
<!-- fill-column: 72 -->
<!-- End: -->
