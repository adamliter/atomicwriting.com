---
date: 2017-09-03 17:00:00 -0400
title: Installing Docker and Docker Compose on Linode
slug: installing-docker-and-docker-compose
tutorial: psiturk-docker-linode
categories:
  - research
  - tech
tags:
  - linode
  - docker
  - experiments
  - mturk
  - psiturk
  - mysql
---

This is the second post
in [a series of blog posts][psiturk-docker-linode]{:target="_blank"}
about how to run an experiment
with [Amazon Mechanical Turk][mturk]{:target="_blank"}, using
[psiTurk]{:target="_blank"}.

This post will cover how to install [Docker][docker]{:target="_blank"}
and [Docker Compose][docker-compose]{:target="_blank"}, which we will
use to run the two programs necessary for deploying an
experiment with psiTurk: MySQL and psiTurk itself.
<!--read more-->

This picks up from the [previous post][previous-post]{:target="_blank"},
where we configured a virtual private server on Linode.

## What is Docker and why use it?

Docker is a software container platform. What this means is that Docker
allows you to run programs inside of containers that are isolated from
the host computer. Docker allows you to specify how exactly to build the
container, which helps ensure that the program you want to run
inside of the container has access to everything it needs.

This is useful for several reasons. For example, it ensures that the
program runs exactly the same way on two completely different computers;
it also allows you to automate the creation of a new development or
production environment. (For more information, head over to [Docker's
website][what-docker]{:target="_blank"}.)

There's also a Docker tool called Docker Compose, which allows you to
run multiple containers on the same host machine in an automated and
coordinated manner. Using Docker Compose will thus allow for deploying
both a database and psiTurk itself on the Linode server that you've
set up.

## The application stack: MySQL and psiTurk

As you may know, with psiTurk it is necessary to hook your experiment
up to a database so that you can record the data from participants as
they participate. By default, psiTurk is configured to use SQLite, which
is not the most robust database. Particularly, it is a bad idea to use
SQLite when you actually deploy your experiment to Amazon Mechanical
Turk. SQLite won't do well if multiple people are doing the experiment
at the same time; you might lose data. Instead, you want to use MySQL,
which is a more robust database.

In addition to running MySQL, you'll also need to run psiTurk itself.
Creating a Docker container for each of these programs will be covered
later in [the series][psiturk-docker-linode]{:target="_blank"}.
This post focuses on installing `docker` and `docker-compose`.

## Installing Docker

If you're not already logged in to your Linode account that you set up
in the [previous post][previous-post]{:target="_blank"}, go ahead and
log in to it using SSH (again, replace the 0's with your server's IP
address and `your_username` with your username):

{% highlight shell %}
ssh your_username@00.000.000.00
{% endhighlight %}

Next, you can install Docker by `curl`-ing and running a script that the
Docker folks maintain for installations:

{% highlight shell %}
curl -sSL https://get.docker.com/ | sh
{% endhighlight %}

At the end, you'll probably see a message suggesting that you add
yourself to the newly created `docker` group. Doing so would allow you
to run `docker` commands without having to use `sudo`. If you do decide
to do this, you can add yourself to the `docker` group by running the
following command (replacing `your_username` with your username):

{% highlight shell %}
sudo usermod -aG docker your_username
{% endhighlight %}

In order for this to take effect, you'll need to log out and log back
in.

Next, you'll need to install Docker Compose. This is done by `curl`-ing
a file from GitHub and saving it to your server, after which you'll need
to give it executable permissions. You'll want to download the latest
version of Docker Compose, which you can find by
following [this link][latest-docker-compose]{:target="_blank"}. After
you follow that link, you should see some commands to execute. At the
time of writing, the most recent version of Docker Compose is `1.15.0`,
so the first command to run is:

{% highlight shell %}
sudo bash -c "curl -L https://github.com/docker/compose/releases/download/1.15.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose"
{% endhighlight %}

If you're reading this in the future, the command will likely be
slightly different in order to reflect the most recent version. You'll
have to change the version number after `/download/` in the
URL. Moreover, note that the command above is slightly different than
what is suggested on
the
[GitHub release page][latest-docker-compose]{:target="_blank"}. There,
they just suggest the part of the command which is in quotes
above. However, since the results of the `curl` are being redirected to
the file `/usr/local/bin/docker-compose`, you'll need `root` permissions
in order to save the file because `/usr/local/bin` is a directory that
is owned by `root`. This isn't quite as simple as just running
`sudo curl ... > /usr/local/bin/docker-comopose` because that only
executes the `curl` command as `root`. It doesn't do the redirecting
(`>`) as `root`. To get the redirect to happen with `root` permissions,
you'll need to run `sudo bash -c "COMMAND HERE"`, replacing
`COMMAND HERE` with the redirected `curl` command.

And just to note one last time: if you're reading this in the future,
you will again want to
follow [this link][latest-docker-compose]{:target="_blank"} in order to
get the latest version.

Next, after `curl`-ing the file, you'll need to make it executable with
the following command:

{% highlight shell %}
sudo chmod +x /usr/local/bin/docker-compose
{% endhighlight %}

Now you should be able to run the `docker-compose` command. Congrats!

In the [next post][next-post]{:target="_blank"} in the
[series][psiturk-docker-linode]{:target="_blank"}, you'll learn how to
set up the psiTurk Docker container.

Please feel free to comment with any questions! And, if you are
finding this information helpful, please consider signing
up for Linode using my [referral link][linode-ref]{:target="_blank"}.
Thanks! 🙇


[mturk]: https://www.mturk.com/mturk/welcome
[psiTurk]: https://psiturk.org/
[docker]: https://www.docker.com/
[docker-compose]: https://docs.docker.com/compose/
[latest-docker-compose]: https://github.com/docker/compose/releases/latest
[what-docker]: https://www.docker.com/what-docker
[linode-ref]: https://www.linode.com/?r=54ae7f8d79dc2dcea5d7778008242b6be864a8cf


[previous-post]: {{ site.baseurl }}{% post_url 2017-08-20-setting-up-linode-for-a-psiturk-experiment %}
[psiturk-docker-linode]: {{ site.baseurl }}/tutorials/psiturk-docker-linode
[next-post]: {{ site.baseurl }}{% post_url 2017-09-17-setting-up-the-psiturk-docker-container %}


<!-- Local Variables: -->
<!-- mode: markdown -->
<!-- coding: utf-8 -->
<!-- fill-column: 72 -->
<!-- End: -->
