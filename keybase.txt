==================================================================
https://keybase.io/adamliter
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://atomicwriting.com
  * I am adamliter (https://keybase.io/adamliter) on keybase.
  * I have a public key ASAMe75UkI-KfRid0Rxumnqd2sRVte4y5a54nKA3Ea_CXgo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0101acc2a3967a5a5e2e6964c625a5f712ae1bcbce418b89a33f056783582c9623350a",
      "host": "keybase.io",
      "kid": "01200c7bbe54908f8a7d189dd11c6e9a7a9ddac455b5ee32e5ae789ca03711afc25e0a",
      "uid": "2141e2128ae5e881b51c324b86636f19",
      "username": "adamliter"
    },
    "merkle_root": {
      "ctime": 1502925870,
      "hash": "ea14564924baa3146ff6903bdff9559b296af6583db0e75ea773e2be4f7c689ab1699fca06ea9f908c32a0b14f0e2cb26c03a0287130d49765d4707dd2cab269",
      "hash_meta": "68a55ebe0ee2fd7253666d8752acea83adf581b072d40f93ed0cdee42aaf36f9",
      "seqno": 1330553
    },
    "service": {
      "hostname": "atomicwriting.com",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.27"
  },
  "ctime": 1502926021,
  "expire_in": 504576000,
  "prev": "0b7a8e5aab70e777c06b8fdaf7ba5c0ad620401d6d96de96a8f673dc00c73eb2",
  "seqno": 29,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgDHu+VJCPin0YndEcbpp6ndrEVbXuMuWueJygNxGvwl4Kp3BheWxvYWTFA0t7ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTAxYWNjMmEzOTY3YTVhNWUyZTY5NjRjNjI1YTVmNzEyYWUxYmNiY2U0MThiODlhMzNmMDU2NzgzNTgyYzk2MjMzNTBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwMGM3YmJlNTQ5MDhmOGE3ZDE4OWRkMTFjNmU5YTdhOWRkYWM0NTViNWVlMzJlNWFlNzg5Y2EwMzcxMWFmYzI1ZTBhIiwidWlkIjoiMjE0MWUyMTI4YWU1ZTg4MWI1MWMzMjRiODY2MzZmMTkiLCJ1c2VybmFtZSI6ImFkYW1saXRlciJ9LCJtZXJrbGVfcm9vdCI6eyJjdGltZSI6MTUwMjkyNTg3MCwiaGFzaCI6ImVhMTQ1NjQ5MjRiYWEzMTQ2ZmY2OTAzYmRmZjk1NTliMjk2YWY2NTgzZGIwZTc1ZWE3NzNlMmJlNGY3YzY4OWFiMTY5OWZjYTA2ZWE5ZjkwOGMzMmEwYjE0ZjBlMmNiMjZjMDNhMDI4NzEzMGQ0OTc2NWQ0NzA3ZGQyY2FiMjY5IiwiaGFzaF9tZXRhIjoiNjhhNTVlYmUwZWUyZmQ3MjUzNjY2ZDg3NTJhY2VhODNhZGY1ODFiMDcyZDQwZjkzZWQwY2RlZTQyYWFmMzZmOSIsInNlcW5vIjoxMzMwNTUzfSwic2VydmljZSI6eyJob3N0bmFtZSI6ImF0b21pY3dyaXRpbmcuY29tIiwicHJvdG9jb2wiOiJodHRwczoifSwidHlwZSI6IndlYl9zZXJ2aWNlX2JpbmRpbmciLCJ2ZXJzaW9uIjoxfSwiY2xpZW50Ijp7Im5hbWUiOiJrZXliYXNlLmlvIGdvIGNsaWVudCIsInZlcnNpb24iOiIxLjAuMjcifSwiY3RpbWUiOjE1MDI5MjYwMjEsImV4cGlyZV9pbiI6NTA0NTc2MDAwLCJwcmV2IjoiMGI3YThlNWFhYjcwZTc3N2MwNmI4ZmRhZjdiYTVjMGFkNjIwNDAxZDZkOTZkZTk2YThmNjczZGMwMGM3M2ViMiIsInNlcW5vIjoyOSwidGFnIjoic2lnbmF0dXJlIn2jc2lnxECkUEV2rROYsklZ+3Fkfh8EPA/Y4I77B9AtpkZzMjGeSEI3ko2CeLErr9Vo2b/01gJZ2Y3k35k/1Yn6pwsVRKQIqHNpZ190eXBlIKRoYXNogqR0eXBlCKV2YWx1ZcQgCEQelteGtjrm8x1ukEh+dUueMIBR9ifsFvP1OX3kpjqjdGFnzQICp3ZlcnNpb24B

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/adamliter

==================================================================