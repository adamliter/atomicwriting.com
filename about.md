---
title: Atomic Writing | About
layout: default
nav_node: about
---

## The content

Atomic Writing is a blog by me, [Adam Liter][me]{:target="_blank"}.
Atomic Writing largely covers technology-related topics 🖥,
but the blog posts are ultimately about whatever I want them to be about.
🎊

## The name

The name for the blog is a play on words.
My first name is pronounced the same way as the word *atom*
([at least in certain dialects of English][flapping]{:target="_blank"},
including my own),
and the adjectival form of *atom* is *atomic*.

## More about me

I'm a linguist with interests in
web development,
programming,
and system administration.
I also **really** like coffee. ☕️
To learn more about me (and/or my academic pursuits),
visit my [personal website][me]{:target="_blank"}.

Other places you can find me include:
<ul>
  <li>
    <a href="{{ site.adamliter_github }}" target="_blank">
      <i class="fa fa-github" aria-hidden="true"></i>
      GitHub
    </a>
  </li>
  <li>
    <a href="{{ site.adamliter_keybase }}" target="_blank">
      <i class="fa fa-key" aria-hidden="true"></i>
      Keybase
    </a>
  </li>
  <li>
    <a href="{{ site.adamliter_se }}" target="_blank">
      <i class="fa fa-stack-exchange" aria-hidden="true"></i>
      Stack Exchange
    </a>
  </li>
</ul>

[me]: {{ site.adamliter_web }}
[flapping]: https://en.wikipedia.org/wiki/Flapping
